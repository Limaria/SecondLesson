﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecondLesson
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();
            int x = rnd.Next(1, 9);
            int y = rnd.Next(1, 9);
            Player value = new Player(x * 2, y);
            value.Graphics();
            Console.WriteLine(x + " " + y);
            Console.ReadKey();
        }
    }

    class Player
    {
        int x;
        int y;
        public Player(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
        
        public void Graphics()
        {
            int m = 0;
            for (int i=0; i<9; i++)
            {
                Console.Write($"{m} ");
                m++;
            }
            m = 1;
            Console.WriteLine("");
            for (int i = 0; i < 9; i++)
            {
                Console.WriteLine(m);
                m++;
            }

            Console.SetCursorPosition(this.x, this.y);
            Console.Write('*');
          
        }
    }
}
